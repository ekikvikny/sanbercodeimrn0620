//No. 1
console.log("No.1");
function arrayToObject(people) {
    var now = new Date();
    var thisYear = now.getFullYear() ;
    var personObj = {};
    if (people == undefined){
        console.log("");
    }else {
        for (var i = 0; i <people.length ;i++ ){
            personObj = {
                firstName : people[i][0],
                lastName: people[i][1],
                gender: people[i][2],
                age: thisYear-people[i][3]
            }
            if (!personObj.age || personObj.age <0){
                personObj.age = "Invalid birth year";
            }
            console.log( (i+1)+". "+personObj.firstName+ ": {" + '\n' +
            "firstname: "+personObj.firstName+","+ '\n' +
            "lastName: "+personObj.lastName+","+ '\n' +
            "gender: "+personObj.gender+","+ '\n' +
            "age: "+personObj.age + '\n' 
            )
        }
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

arrayToObject([]) 

//No. 2
console.log("No.2");
function shoppingTime(memberId, money) {
    var Sale = [
        ["Sepatu Stacattu", 1500000], 
        ["Baju Zoro", 500000], 
        ["Baju H&N", 250000], 
        ["Sweater Uniklooh", 175000], 
        ["Casing Handphone", 50000]
    ];
    var sisauang = money;
    var barangBelanja = [];
    if (memberId == null || memberId == ''){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }else if (money < 50000){
        return "Mohon maaf, uang tidak cukup";
    }else {
        for (var i = 0; i < Sale.length; i++){
            if (Sale[i][1] <= sisauang){
                sisauang -= Sale[i][1];
                barangBelanja.push(Sale[i][0]);
            } 
        }
    }
     var barang = {
            memberId: memberId,
            money: money,
            listPurchased: barangBelanja,
            changeMoney: sisauang
        }
    return barang;
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000)); 
console.log(shoppingTime()); 

//No. 3
console.log('\n'+"No.3");
function naikAngkot(listPenumpang){
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var angkot = [];
    if (listPenumpang == undefined || listPenumpang.length ==0){
        return listPenumpang;
    }else {
        for (var i = 0; i < listPenumpang.length; i++){
            var tarif = (rute.indexOf(listPenumpang[i][2]) - rute.indexOf(listPenumpang[i][1])) * 2000
            var penumpang = {
                penumpang: listPenumpang[i][0],
                naikDari: listPenumpang[i][1],
                tujuan: listPenumpang[i][2],
                bayar: tarif
            }
            angkot.push(penumpang);
        }
        return angkot;
    }
    
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([])); 
