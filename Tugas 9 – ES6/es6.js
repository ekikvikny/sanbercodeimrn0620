//No. 1
console.log('\n'+"No .1")
golden = () => {
    console.log("this is golden!!")
 };
 golden()
 
 //No. 2
console.log('\n'+"No .2")
const newFunction = function literal(firstName, lastName){
    return {
        fullName(){
            console.log(`${firstName} ${lastName}`)
            return 
        }
    }
  }
  newFunction("William", "Imoh").fullName()

//No. 3
console.log('\n'+"No .3")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const {firstName, lastName, destination, occupation, spell} = newObject
console.log(firstName, lastName, destination, occupation)

//No. 4
console.log('\n'+"No .4")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east]
console.log(combined)

//No. 5
console.log('\n'+"No .5")
const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet,   
    consectetur adipiscing elit, ${planet} do eiusmod tempor 
    incididunt ut labore et dolore magna aliqua. Ut enim
    ad minim veniam`
 
console.log(before) 
