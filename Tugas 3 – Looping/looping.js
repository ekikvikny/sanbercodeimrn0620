//No. 1
console.log("No.1");
var iterasi = 1;
console.log("Looping Pertama");
while(iterasi <= 20) { 
    if(iterasi % 2 == 0){
        console.log(iterasi+" - I love coding"); 
        iterasi++;
    }else {
        iterasi++;
    }
    
     
}
var iterasi = 20;
console.log("Looping Kedua");
while(iterasi >= 1) { 
    if(iterasi % 2 == 0){
        console.log(iterasi+" - I will become a mobile developer"); 
        iterasi--;
    }else {
        iterasi--;
    }
    
     
}

//No. 2
console.log("No.2");
for(var angka = 1; angka <= 20; angka++) {
    if (angka % 2 == 0){
        console.log(angka + " - Berkualitas");
    }else if (angka %2 !== 0 && angka % 3 == 0   ){
        console.log(angka + " - I Love Coding");
    }else {
        console.log(angka + " - Santai");
    }
} 

//No. 3
console.log("No.3");
var kolom = 8;
var baris = 4;
var hash = '';
for(var i = 1; i <= baris; i++) {
    for(var j = 1; j <= kolom; j++) {
        hash += '#';
    }
    hash += '\n';
} 
console.log(hash);

//No. 4
console.log("No.4");
var n = 7;
var hash2 = '';
for(var i = 1; i <= n; i++) {
    for(var j = 1; j <= n; j++) {
        if(j >= n - i +1){
            hash2 += '#' ;
        }
    }
    hash2 += '\n';
} 
console.log(hash2);

//No. 5
console.log("No.5");
var a = 8;
var hash3 = '';
for(var i = 1; i <= a; i++) {
    for(var j = 1; j <= a; j++) {
        if( j%2 != 0 && i%2 ==0){
            hash3 += '#' ;
        }else if (j%2 == 0 && i%2 != 0){
            hash3 += '#';
        }else {
            hash3 += ' ';
        }
    }
    hash3 += '\n';
} 
console.log(hash3);
