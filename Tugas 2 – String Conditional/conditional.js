//if-else
console.log("if-else")

var nama = "Budi";
var peran = "Penyihir";

if (!nama){
    console.log("Nama Harus diisi");
}
else if (nama.length > 0 ){
    if (!peran){
        console.log("Halo "+nama+", Pilih peranmu untuk memulai game!");
    }
    else if(peran === "Penyihir"){
        console.log("Halo "+nama+", Selamat Datang di Werewolf ");
        console.log("kamu adalah Penyihir, skillmu dapat melihat siapa yang menjadi werewolf!");
    }
    else if(peran === "Guard"){
        console.log("Halo "+nama+", Selamat Datang di Werewolf ");
        console.log("kamu adalah Guard, skillmu akan membantu melindungi temanmu dari serangan werewolf.");
    }
    else if(peran === "Werewolf"){
        console.log("Halo "+nama+", Selamat Datang di Werewolf ");
        console.log("kamu adalah Werewolf, Kamu akan memakan mangsa setiap malam!");
    }
    
}
console.log("");

//Switch case
console.log("Switch case");

var tanggal = 25; 
var bulan = 7; 
var tahun = 1997;

switch(bulan){
    case 1 : {bulan = "Januari"};break;
    case 2 : {bulan = "Februari"};break;
    case 3 : {bulan = "Maret"};break;
    case 4 : {bulan = "April"};break;
    case 5 : {bulan = "Mei"};break;
    case 6 : {bulan = "Juni"};break;
    case 7 : {bulan = "Juli"};break;
    case 8 : {bulan = "Agustus"};break;
    case 9 : {bulan = "September"};break;
    case 10 : {bulan = "Oktober"};break;
    case 11 : {bulan = "November"};break;
    case 12 : {bulan = "Desember"};break;
}
if ((tanggal >=1 && tanggal <= 31) && (tahun >= 1900 && tahun <=2200)){
    console.log(tanggal+" "+bulan+" "+tahun);
}else {
    console.log("input tanggal atau tahun ada yang salah");
}




