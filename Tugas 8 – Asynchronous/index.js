var readBooks = require('./callback.js');
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var timeavail = 10000;


function bookRead(index,times){
    if(index >= books.length){
        return console.log(" ");

    }
    else{
        readBooks(times,books[index],function(times){
            return index + bookRead(index+1,times)
        })
    }
}
bookRead(0,timeavail);
