var readBooksPromise = require('./promise.js');
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var timeavail = 10000;

function readBooks(index, time){
    if(index >= books.length){
        return console.log(" ");
    }
    else{
        readBooksPromise(time,books[index])
        .then(function(times){
            return index + readBooks(index+1,times);
        })
        .catch(function(error){
            return error ;
        })
    }
}
readBooks(0,timeavail);
