import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import { Login, Profile, Skill, Project, Add, Root } from "./NavScreen";

const AuthStack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const RootScreen = () => (
  <AuthStack.Navigator>
      <AuthStack.Screen name="Root" component={Root} options={{ title: 'Root' }} />
      <AuthStack.Screen name="Login" component={Login} options={{ title: 'Login' }} />
    </AuthStack.Navigator>
)
const Tabsscreen = () => (
    <Tab.Navigator>
          <Tab.Screen name="Skill" component={Skill} />
          <Tab.Screen name="Project" component={Project} />
          <Tab.Screen name="Add" component={Add} />
        </Tab.Navigator>
)
export default() => (
    <NavigationContainer>
      <Drawer.Navigator>
        <Drawer.Screen name="Root" component={RootScreen} />
        <Drawer.Screen name="About" component={Profile} options={{ title: 'About' }} />
        <Drawer.Screen name="Tabs" component={Tabsscreen} />
      </Drawer.Navigator>
      
    </NavigationContainer>
);
