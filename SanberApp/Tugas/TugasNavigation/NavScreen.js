import React from "react";
import { View, Text, StyleSheet, Button } from "react-native";
import LoginScreen from "./../Tugas13/LoginScreen";
import AboutScreen from "./../Tugas13/AboutScreen";
import SkillScreen from "./../Tugas14/SkillScreen";
import ProjectScreen from "./ProjectScreen";
import AddScreen from "./AddScreen";


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginVertical: 10,
    borderRadius: 5
  }
});

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Root = ({ navigation }) => (
  <ScreenContainer>
    <Button title="Login" onPress={() => navigation.push("Login", { name: "Login" })} />
    <Button title="Drawer" onPress={() => navigation.toggleDrawer()}/>
  </ScreenContainer>
);

export const Login = ({ navigation }) => {
  return (
    <ScreenContainer>
      <LoginScreen />
    </ScreenContainer>
    
  );
};

export const Profile = ({ navigation }) => {
  return (
    <ScreenContainer>
        <AboutScreen />
    </ScreenContainer>
  );
};

export const Skill = ({ navigation }) => {
    return (
      <ScreenContainer>
        <SkillScreen />
      </ScreenContainer>
      
    );
  };

  export const Project = ({ navigation }) => {
    return (
      <ScreenContainer>
        <ProjectScreen />
      </ScreenContainer>
      
    );
  };

  export const Add = ({ navigation }) => {
    return (
      <ScreenContainer>
        <AddScreen />
      </ScreenContainer>
      
    );
  };

