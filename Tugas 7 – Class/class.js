//No. 1
console.log('\n'+"No. 1");
class Animal {    
    constructor(name) {
        this.aniname = name;
        this.legs = 4;
        this.cold_blooded= false ;
    }
    get name (){
        return this.aniname;
    }
}
 
console.log("Release 0");
var sheep = new Animal("shaun");
 
console.log(sheep.name) 
console.log(sheep.legs) 
console.log(sheep.cold_blooded) 

class Ape extends Animal {
    constructor(name){
        super(name);
        this.legs = 2;   
    }
    yell() {
        return  console.log("Auooo");
    }
}
class Frog extends Animal {
    constructor(name){
        super(name);
    }
    jump() {
        return  console.log("hop hop");
    }
}
 
console.log("Release 1");
var sungokong = new Ape("kera sakti");
sungokong.yell() ;
 
var kodok = new Frog("buduk");
kodok.jump() ;

//No. 2
console.log('\n'+"No. 2");
class Clock {
    constructor({template}) {
      this.template=template;
      this.timer;
    }

    render() {
        var date = new Date();
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
    
        var output = this.template
          .replace('h', hours)
          .replace('m', mins)
          .replace('s', secs);
    
        console.log(output);
    
    }

    stop() {
      clearInterval(this.timer);
    }

    start(){
        this.render();
        this.timer = setInterval(() => this.render(), 1000);    
    }

  }

  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 
