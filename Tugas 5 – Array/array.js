//No.1
console.log("No. 1")
function range(startNum, finishNum) {
    var num = [];
    if (finishNum >= startNum){
        for (i=startNum ;i<=finishNum;i++){
            num.push(i);
        }
       return num.sort(function (startNum, finishNum) { return startNum - finishNum } ) ;
    }else if (startNum > finishNum) {
        for (i=finishNum ;i<=startNum;i++){
            num.push(i);
        }
        return num.sort(function (startNum, finishNum) { return  finishNum - startNum } ) ;
    }else {
        return -1;
    }

}
console.log(range(1,10)); 

//No. 2
console.log("No. 2")
function rangeWithStep(startNum, finishNum, step) {
    var num = [];
    if (finishNum >= startNum){
        for (i=startNum ;i<=finishNum;i+=step){
            num.push(i);
        }
       return num.sort(function (startNum, finishNum) { return startNum - finishNum } ) ;
    }else if (startNum > finishNum) {
        for (i=startNum ;i>=finishNum;i-=step){
            num.push(i);
        }
        return num.sort(function (startNum, finishNum) { return  finishNum - startNum } ) ;
    }else {
        return -1;
    }

}
console.log(rangeWithStep(29, 2, 4))

//No. 3
console.log("No. 3")
function sum(startNum, finishNum,step) {
    if (step != true){
        var total = rangeWithStep(startNum, finishNum, 1).reduce(function(a, b){ return a + b;}, 0);
    }else {
        var total = rangeWithStep(startNum, finishNum, step).reduce(function(a, b){ return a + b;}, 0);
    }
    return total;
}
console.log(sum(1,10))

//No. 4
console.log("No. 4")
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] ;
function dataHandling(inputan) {
    for (index = 0; index < inputan.length; index++) {
        console.log("Nomor ID: "+inputan[index][0]);
        console.log("Nama Lengkap: " +inputan[index][1]);
        console.log("TTL: " +inputan[index][2] +" "+ inputan[index][3]);
        console.log("Hobi: "+inputan[index][4]);
        console.log();
    }
}
dataHandling(input);

//No. 5
console.log("No. 5")
function balikKata(string) {
    var kata_terbalik = "";
    for (index = string.length; index >= 0; index--) {
        kata_terbalik += string.charAt(index) ;
    }
    return kata_terbalik;
}
console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode")) ;
console.log(balikKata("Haji Ijah")) ;
console.log(balikKata("racecar")) ;
console.log(balikKata("I am Sanbers")) ;

//No. 6
console.log("No. 6")
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(input) {
    input.splice(1, 1, input[1] + "Elsharawy");
    input.splice(2, 1, "Provinsi " + input[2]);
    input.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(input);

    var date = input[3].split('/')
	bulan = parseInt(date[1]);
	switch(bulan){
		case 1:   { var bulan = "Januari"; break; }
		case 2:   { var bulan = "Februari"; break; }
		case 3:   { var bulan = "Maret"; break; }
		case 4:   { var bulan = "April"; break; }
		case 5:   { var bulan = "Mei"; break; }
		case 6:   { var bulan = "Juni"; break; }
		case 7:   { var bulan = "Juli"; break; }
		case 8:   { var bulan = "Agustus"; break; }
		case 9:   { var bulan = "September"; break; }
		case 10:   { var bulan = "Oktober"; break; }
		case 11:   { var bulan = "November"; break; }
		case 12:   { var bulan = "Desember"; break; }
		default:   { var bulan = null; break; }
	}
    console.log(bulan);
    console.log(date.slice().sort(function (a, b) { return b-a }));
    console.log(date.join("-"));
    console.log(input[1].slice(0, 14));
}
dataHandling2(input);

